# TODO

- [x] Add a `.gitignore`
- [x] Add a `README.md`
- [x] Add a `LICENSE`
- [ ] Add a `CONTRIBUTING.md`
- [x] Add a `TODO.md`
- [ ] Add a `CHANGELOG.md`
- [ ] Add a `Makefile`
- [ ] Integrate with `youtube-dl`
