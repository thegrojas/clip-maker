<h1 align="center">Clip Maker</h1>

<div align="center">

[![Status](https://img.shields.io/badge/status-active-success.svg)]() 
[![Version](https://img.shields.io/badge/version-0.0.1-informational)](./CHANGELOG.md) 
[![License](https://img.shields.io/badge/license-GPLv3-blue.svg)](./LICENSE)

</div>

---

<p align="center">Python script that automates creating multiple clips out of a video file. It uses a simple Yaml file to define the start time, end time and tempo of all your clips.
    <br> 
</p>

<p align="center">
  <a href="#about">🧐 About</a> •
  <a href="#getting_started">🏁 Getting Started</a> •
  <a href="#usage">🤹 Usage</a> •
  <a href="#changelog">📜 Changelog</a> •
  <a href="#todo">✅ Todo</a> •
  <a href="#built_using">🔨 Built Using</a> •
  <a href="#contributing">⚙️ Contributing</a> •
  <a href="#credits">✍️ Credits</a> •
  <a href="#support">🤲 Support</a> •
  <a href="#license">⚖️ License</a>
</p>

![Screenshot](./screenshot.png)

## 🧐 About <a name="about"></a>

I like to add video clips to my [Anki](https://apps.ankiweb.net/) flashcards. Creating clips out of the source video one by one became tedious, and I couldn't find another tool that was simple and efficient at what I was looking for (except for some others ffmpeg scripts that lack my needs).

This is a Python script that automates creating multiple clips out of a video file. It uses a simple Yaml file to define the start time, end time and tempo of all your clips.

## 🏁 Getting Started <a name="getting_started"></a>

Just make sure you cover the Prerequisites and Installing sections, then jump to [Usage](#usage) where I will demonstrate how to use the script.

### 🦺 Prerequisites

**clip-maker** only has 2 dependencies that you have to install.

- [ffmpeg](https://ffmpeg.org/)

```sh
sudo apt install ffmpeg
```

- [PyYAML](https://pyyaml.org/)

```sh
pip3 install pyyaml
```

### 📦 Installing

No special installation steps are needed, just clone this repo wherever you like:

```sh
git clone https://gitlab.com/thegrojas/clip-maker.git
cd clip-maker
```

## 🤹 Usage <a name="usage"></a>

Let's say that I want clips of all the sections where an animal is injured on the movie [Big Buck Bunny](https://peach.blender.org/):

<iframe src="https://player.vimeo.com/video/1084537" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/1084537">Big Buck Bunny</a> from <a href="https://vimeo.com/yourown3dsoftware">Blender</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

I already downloaded that movie on my computer as a *.webm* video file (I'm using webm as an example but any video format should work).

Now I have to move the movie to the folder containing **clip-maker** and give it a name that's easier to work with:

```
mv path/to/my/downloaded/video.webm /path/to/py-videocutter/bigbuckbunny.webm
```

Checking the contents of my **clip-maker** folder I see a *.yml* file, a *.py* file and the video that I downloaded before.

Almost there! Now I have to edit the contents of the file called *config.yml* to my liking, here is an example of how the default one looks like (thats the one we will use for this example):

```yaml
---

source:
  file: 'bigbuckbunny.webm' # Name of the file in the current folder
  # url: 'https://www.youtube.com/watch?v=aqz-KE-bpKQ' # You can remove this line for now. This a feature that it is in the works.
clips:
    - 'start': '00:00:15' # This clip will go from 20 seconds to 26 seconds and will have a normal speed.
      'end': '00:00:23'
      'tempo': '1'
      'name': 'bird-rock'
    - 'start': '00:05:28' # This clip will go from 5:28 minutes to 5:43 minutes and will go on slow motion.
      'end': '00:05:43'
      'tempo': '2'
      'name': 'squirrel-log'
    - 'start': '00:05:47' # This clip will go from 5:47 to 6:21 and will be accelerated.
      'end': '00:06:21'
      'tempo': '0.5'
      'name': 'squirrel-launch'

...
```

You can see that I will make 3 clips with their respective `start` time, `end` time, `tempo` and `name`.

I make sure that `clip-maker` is set as executable: 

```sh
chmod +x clip-maker
```

Now just run `clip-maker` and wait for it to finish:

```sh
./clip-maker
```

## 📜 Changelog <a name="changelog"></a>

All notable changes to this project will be documented in `CHANGELOG.md`. Click [here](./CHANGELOG.md) to take a look.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## ✅ Todo <a name="todo"></a>

Pending tasks will be tracked in `TODO.md`. Click [here](./TODO.md) to take a look.

## 🔨 Built Using <a name="built_using"></a>


- [Python](https://www.python.org/) - Scripting Language
- [Python PyYaml](https://pyyaml.org/) - Yaml Parser
- [Ffmpeg](https://ffmpeg.org/) - Video Processing

## ⚙️ Contributing <a name="contributing"></a>

Developers, Testers and Translators are all welcomed. For more information on how to contribute to this project please refer to `CONTRIBUTING.md`. Click [here](./CONTRIBUTING.md) to take a look.

## ✍️ Credits <a name="credits"></a>

- To that person who has a gist of a similar script, which I can no longer find and acknowledge properly.
- [@kylelobo](https://github.com/kylelobo) creator of [The Documentation Compendium](https://github.com/kylelobo/The-Documentation-Compendium) whose templates I used as inspiration for my README's.

See also the list of [contributors](#) who participated in this project.

## 🤲 Support <a name="support"></a>

For now, just by sharing and/or staring this repository you are supporting my work.

## ⚖️ License <a name="license"></a>

This code is released under the [GLPv3](./LICENSE) license. For more information refer to `LICENSE.md`
